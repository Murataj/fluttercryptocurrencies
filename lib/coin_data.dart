//TODO: Add your imports here.
import 'package:http/http.dart' as http;
import 'dart:convert';

const List<String> currenciesList = [
  'AUD',
  'BRL',
  'CAD',
  'CNY',
  'EUR',
  'GBP',
  'HKD',
  'IDR',
  'ILS',
  'INR',
  'JPY',
  'MXN',
  'NOK',
  'NZD',
  'PLN',
  'RON',
  'RUB',
  'SEK',
  'SGD',
  'USD',
  'ZAR'
];

const List<String> cryptoList = [
  'BTC',
  'ETH',
  'LTC',
];

const bitcoinAverageURL =
    'https://apiv2.bitcoinaverage.com/indices/global/ticker';

class CoinData {
  //TODO: Create your getCoinData() method here.

  CoinData(this.url);
  final String url;

  Future getCoinData() async {
  http.Response response = await http.get(url);
  if (response.statusCode == 200) {
  String data = response.body;
  return jsonDecode(data);
  } else {
  print(response.statusCode);
  }
  }
}


class Bitcoin {

  Future<dynamic> bitcoinPrice(String currency) async {
    CoinData networkhelper = CoinData(
        'https://apiv2.bitcoinaverage.com/indices/global/ticker/BTC$currency');

    var bitcoinPrice = await networkhelper.getCoinData();
    return bitcoinPrice;
  }

  Future<dynamic> etheriumPrice(String currency) async {
    CoinData networkhelper = CoinData(
        'https://apiv2.bitcoinaverage.com/indices/global/ticker/ETH$currency');

    var bitcoinPrice = await networkhelper.getCoinData();
    return bitcoinPrice;
  }

  Future<dynamic> literiumPrice(String currency) async {
    CoinData networkhelper = CoinData(
        'https://apiv2.bitcoinaverage.com/indices/global/ticker/LTC$currency');

    var bitcoinPrice = await networkhelper.getCoinData();
    return bitcoinPrice;
  }

}